/*
 * TcpIp stub driver - Used to monitor+populate Tcpip ipv4 registry data.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */
#include <stdarg.h>
#include <assert.h>
#include "ntstatus.h"
#define WIN32_NO_STATUS
#include "winternl.h"
#include <winsock2.h>
#include <iphlpapi.h>
#include <ws2tcpip.h>
#include <netioapi.h>
#include "ddk/wdm.h"
#include "wine/debug.h"
#include "wine/heap.h"

WINE_DEFAULT_DEBUG_CHANNEL(tcpip);

static HANDLE monitor_thread, exit_event;

static void add_tcp_ipv4_network_interface_registry_value(const CHAR *guidstrA, const DWORD type, const WCHAR *name, const WCHAR *value, DWORD value_size)
{
    HKEY key;
    WCHAR keynameW[148];

    swprintf( keynameW, ARRAY_SIZE(keynameW), L"System\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\%hs", guidstrA);
    if (RegCreateKeyExW( HKEY_LOCAL_MACHINE, keynameW, 0, NULL,
                 REG_OPTION_VOLATILE, KEY_ALL_ACCESS, NULL, &key, NULL ) == ERROR_SUCCESS)
    {
		if (value_size == 0)
			value_size = (lstrlenW(value) + 1) * sizeof(WCHAR);

		RegSetValueExW( key, name, 0, type, (BYTE *)value, value_size );
		RegCloseKey( key );
    }
}

static BOOL get_buffer(const DWORD new_size, DWORD* current_size, WCHAR** current_buffer)
{
    BOOL result = FALSE;

    if ((current_size != NULL) && (current_buffer != NULL))
    {
        if (new_size > *current_size)
        {
            WCHAR* new_buffer = (WCHAR*)HeapReAlloc(GetProcessHeap(), 0, *current_buffer, new_size);
            if (new_buffer != NULL)
            {
                *current_buffer = new_buffer;
                *current_size = new_size;
                result = TRUE;
            }
        }
        else
        {
            result = TRUE;
        }
    }

    return result;
}

static PIP_ADAPTER_ADDRESSES g_cached_aa = NULL;
static ULONG g_cached_aasize = 0;

static void populate_tcp_ipv4_network_interface_registry_data(void)
{
    BYTE dummy;
	PIP_ADAPTER_ADDRESSES aa = NULL;
    DWORD call_result;
	PIP_ADAPTER_ADDRESSES caa = NULL;
	ULONG aasize = 1;
    WCHAR value[32];
    WCHAR* reg_buffer = NULL;
    DWORD reg_buffer_size = 0;
    WCHAR* reg_buffer2 = NULL;
    DWORD reg_buffer2_size = 0;
	DWORD total_used_value_size;
	DWORD total_used_value2_size;
    DWORD value_size;
    struct sockaddr_in *sa = NULL;
    PIP_ADAPTER_UNICAST_ADDRESS ua = NULL;
	PIP_ADAPTER_GATEWAY_ADDRESS ga = NULL;
    PIP_ADAPTER_DNS_SERVER_ADDRESS dsa = NULL;
    struct in_addr a_mask_addr;

	call_result = GetAdaptersAddresses( AF_INET,
										GAA_FLAG_SKIP_ANYCAST | GAA_FLAG_SKIP_MULTICAST | GAA_FLAG_INCLUDE_ALL_GATEWAYS, (void*)0x42DEABB8,
										(PIP_ADAPTER_ADDRESSES)&dummy, &aasize );

	aa = (PIP_ADAPTER_ADDRESSES)HeapAlloc( GetProcessHeap(), 0, aasize );
	if (aa == NULL)
		return;

	for (;;)
	{
		call_result = GetAdaptersAddresses( AF_INET,
										GAA_FLAG_SKIP_ANYCAST | GAA_FLAG_SKIP_MULTICAST | GAA_FLAG_INCLUDE_ALL_GATEWAYS, (void*)0x42DEABB8,
										aa, &aasize );
		if (call_result == 0)
			break;

		if (call_result == ERROR_BUFFER_OVERFLOW)
		{
			/* In extreme cases GetAdaptersAddresses could fail if certain network information changes within the call.
			So, realloc and try again.*/
			caa = HeapReAlloc( GetProcessHeap(), 0, aa, aasize );
			if (!caa)
				break;
			aa = caa;
		}
		else
			break;
	}

	if (g_cached_aa && g_cached_aasize == aasize && !memcmp( g_cached_aa, aa, aasize ))
	{
		HeapFree( GetProcessHeap(), 0, aa );
		return;
	}

	reg_buffer = (WCHAR*)HeapAlloc( GetProcessHeap(), 0, 32 );
	if (reg_buffer == NULL)
		goto cleanup;

	reg_buffer2 = (WCHAR*)HeapAlloc( GetProcessHeap(), 0, 32 );
	if (reg_buffer2 == NULL)
		goto cleanup;

	reg_buffer_size = 32;
	reg_buffer2_size = 32;

    if (call_result == 0)
	{
		caa = aa;
		while (caa != NULL)
		{
			// Ignore loopback.
			if (caa->IfType != IF_TYPE_SOFTWARE_LOOPBACK)
			{
				total_used_value_size = 0;
				total_used_value2_size = 0;

				memset( reg_buffer, 0, reg_buffer_size );
				memset( reg_buffer2, 0, reg_buffer2_size );

				// IP - IPAddress
				for (ua = caa->FirstUnicastAddress; ua != NULL; ua = ua->Next)
				{
                    sa = (struct sockaddr_in *)( ua->Address.lpSockaddr );

                    memset( value, 0, sizeof(value) );

                    InetNtopW( AF_INET, &(sa->sin_addr), value, ARRAY_SIZE(value) - 1 );

                    value_size = (lstrlenW( value ) + 1) * sizeof(WCHAR);

                    if (!get_buffer( total_used_value_size + value_size, &reg_buffer_size, &reg_buffer ))
                        break;

					memcpy( &reg_buffer[total_used_value_size / sizeof(WCHAR)], value, value_size );

					total_used_value_size += value_size;

					memset( &a_mask_addr, 0, sizeof(a_mask_addr) );

                    ConvertLengthToIpv4Mask( ua->OnLinkPrefixLength, &a_mask_addr.S_un.S_addr );

                    memset( value, 0, sizeof(value) );

                    InetNtopW( AF_INET, &(a_mask_addr), value, ARRAY_SIZE(value) - 1 );

                    value_size = (lstrlenW( value ) + 1) * sizeof(WCHAR);

                    if (!get_buffer( total_used_value2_size + value_size, &reg_buffer2_size, &reg_buffer2 ))
                        break;

					memcpy( &reg_buffer2[total_used_value2_size / sizeof(WCHAR)], value, value_size );

					total_used_value2_size += value_size;
				}

				value[0] = L'\0';

				if (!get_buffer( total_used_value_size + sizeof(WCHAR), &reg_buffer_size, &reg_buffer ))
					break;

				if (!get_buffer( total_used_value2_size + sizeof(WCHAR), &reg_buffer2_size, &reg_buffer2 ))
					break;

				memcpy( &reg_buffer[total_used_value_size / sizeof(WCHAR)], value, sizeof(WCHAR) );
				memcpy( &reg_buffer2[total_used_value2_size / sizeof(WCHAR)], value, sizeof(WCHAR) );

				add_tcp_ipv4_network_interface_registry_value( caa->AdapterName, REG_MULTI_SZ, L"IPAddress", reg_buffer, total_used_value_size + sizeof(WCHAR) );
				add_tcp_ipv4_network_interface_registry_value( caa->AdapterName, REG_MULTI_SZ, L"SubnetMask", reg_buffer2, total_used_value2_size + sizeof(WCHAR) );

				total_used_value_size = 0;

				// GW - DefaultGateway
				for (ga = caa->FirstGatewayAddress; ga != NULL; ga = ga->Next)
				{
					sa = (struct sockaddr_in *)(ga->Address.lpSockaddr);

                    memset( value, 0, sizeof(value) );

                    InetNtopW( AF_INET, &(sa->sin_addr), value, ARRAY_SIZE(value) - 1 );

                    value_size = (lstrlenW( value ) + 1) * sizeof(WCHAR);

                    if (!get_buffer( total_used_value_size + value_size, &reg_buffer_size, &reg_buffer ))
                        break;

					memcpy( &reg_buffer[total_used_value_size / sizeof(WCHAR)], value, value_size );

					total_used_value_size += value_size;
				}

				value[0] = L'\0';

				if (!get_buffer( total_used_value_size + sizeof(WCHAR), &reg_buffer_size, &reg_buffer ))
					break;

				memcpy( &reg_buffer[total_used_value_size / sizeof(WCHAR)], value, sizeof(WCHAR) );

				add_tcp_ipv4_network_interface_registry_value( caa->AdapterName, REG_MULTI_SZ, L"DefaultGateway", reg_buffer, total_used_value_size + sizeof(WCHAR) );

				total_used_value_size = 0;

				// DNS - NameServer
				for (dsa = caa->FirstDnsServerAddress; dsa != NULL; dsa = dsa->Next)
				{
					sa = (struct sockaddr_in *)(dsa->Address.lpSockaddr);

					memset( value, 0, sizeof(value) );

                    InetNtopW( AF_INET, &(sa->sin_addr), value, ARRAY_SIZE(value) );

					value_size = lstrlenW( value ) * sizeof(WCHAR);

                    if (!get_buffer( total_used_value_size + value_size, &reg_buffer_size, &reg_buffer ))
                        break;

					memcpy( &reg_buffer[total_used_value_size / sizeof(WCHAR)], value, value_size );

					total_used_value_size += value_size;

					if (dsa->Next != NULL)
					{
						value[0] = L',';
						value_size = sizeof(WCHAR);

						if (!get_buffer( total_used_value_size + value_size, &reg_buffer_size, &reg_buffer ))
							break;

						memcpy( &reg_buffer[total_used_value_size / sizeof(WCHAR)], value, value_size );

						total_used_value_size += value_size;
					}
				}

				value[0] = L'\0';

				if (!get_buffer( total_used_value_size + sizeof(WCHAR), &reg_buffer_size, &reg_buffer ))
					break;

				memcpy( &reg_buffer[total_used_value_size / sizeof(WCHAR)], value, sizeof(WCHAR) );

				add_tcp_ipv4_network_interface_registry_value( caa->AdapterName, REG_SZ, L"NameServer", reg_buffer, total_used_value_size );
			}

			caa = caa->Next;
		}
	}

	if (g_cached_aa)
		caa = HeapReAlloc( GetProcessHeap(), 0, g_cached_aa, aasize );
	else
		caa = HeapAlloc( GetProcessHeap(), 0, aasize );
	if (caa)
	{
		g_cached_aa = caa;
		g_cached_aasize = aasize;
		memcpy( g_cached_aa, aa, aasize );
	}

cleanup:

	if (reg_buffer != NULL)
		HeapFree( GetProcessHeap(), 0, reg_buffer );

	if (reg_buffer2 != NULL)
		HeapFree( GetProcessHeap(), 0, reg_buffer2 );

	if (aa != NULL)
		HeapFree( GetProcessHeap(), 0, aa );
}

static DWORD WINAPI monitor_thread_proc(void *arg)
{
    struct sockaddr_in bindAddress;
    SOCKET sock = INVALID_SOCKET;
    HANDLE sock_evt = NULL;
	WSAOVERLAPPED sock_ov = {0};
    HANDLE events[3];
	HANDLE refresh_req_event, refresh_req_done_event, refresh_req_mx;
	int ret;
	u_long opt_val;
	DWORD num_bytes;
	DWORD le;
    DWORD wr;

    TRACE("Starting monitor thread.\n");

	refresh_req_event = CreateEventA( NULL, FALSE, FALSE, "Global\\TCPSysRefreshReqEvt" );
	refresh_req_done_event = CreateEventA( NULL, FALSE, FALSE, "Global\\TCPSysRefreshReqDoneEvt" );
	refresh_req_mx = CreateMutexA(NULL, FALSE, "Global\\TCPSysRefreshReqMx" );

	bindAddress.sin_family = AF_INET;
    bindAddress.sin_addr.s_addr = INADDR_ANY;

    sock = WSASocketA( AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED );
	if (sock == INVALID_SOCKET)
	{
        ERR("WSASocketW fail = 0x%08X\n", WSAGetLastError());
		goto cleanup;
	}

	ret = bind( sock, (struct sockaddr*)&bindAddress, sizeof(bindAddress) );
	if (ret == -1)
	{
        ERR("bind fail = 0x%08X\n", WSAGetLastError());
		goto cleanup;
	}

	opt_val = 1;
    ret = ioctlsocket( sock, FIONBIO, &opt_val );

	if (ret == -1)
	{
        ERR("ioctlsocket(FIONBIO) fail = 0x%08X\n", WSAGetLastError());
		goto cleanup;
	}

	sock_evt = CreateEventA( NULL, FALSE, FALSE, NULL );
	if (sock_evt == NULL)
	{
        ERR("CreateEvent fail = 0x%08X\n", GetLastError());
		goto cleanup;
	}

	sock_ov.hEvent = sock_evt;

	ret = WSAIoctl( sock, SIO_ADDRESS_LIST_CHANGE, NULL, 0, NULL, 0, &num_bytes, &sock_ov, NULL );
	if (ret != -1)
	{
        ERR("WSAIoctl(SIO_ADDRESS_LIST_CHANGE) fail = 0x%08X\n", GetLastError());
		goto cleanup;
	}
	else
	{
		le = GetLastError();
		if (le != ERROR_IO_PENDING)
		{
            ERR("WSAIoctl(SIO_ADDRESS_LIST_CHANGE) fail2 = 0x%08X\n", le);
			goto cleanup;
		}
	}

    events[0] = sock_evt;
	events[1] = refresh_req_event;
    events[2] = exit_event;

	for (;;)
	{
		wr = WaitForMultipleObjects( 3, events, FALSE, INFINITE );
		if (wr == WAIT_OBJECT_0)
		{
			populate_tcp_ipv4_network_interface_registry_data();
			SetEvent( refresh_req_done_event );

			memset( &sock_ov, 0, sizeof(sock_ov) );

			sock_ov.hEvent = sock_evt;

			ret = WSAIoctl( sock, SIO_ADDRESS_LIST_CHANGE, NULL, 0, NULL, 0, &num_bytes, &sock_ov, NULL );
			if (ret != -1)
			{
                ERR("WSAIoctl(SIO_ADDRESS_LIST_CHANGE) rearm fail = 0x%08X\n", GetLastError());
				goto cleanup;
			}
			else
			{
				le = GetLastError();
				if (le != ERROR_IO_PENDING)
				{
                    ERR("WSAIoctl(SIO_ADDRESS_LIST_CHANGE) rearm fail2 = 0x%08X\n", le);
					goto cleanup;
				}
			}
		}
		else if (wr == (WAIT_OBJECT_0 + 1))
		{
			populate_tcp_ipv4_network_interface_registry_data();
			SetEvent( refresh_req_done_event );
		}
		else if (wr == (WAIT_OBJECT_0 + 2))
		{
            break;
		}
		else
		{
            ERR("WFMO fail = 0x%08X, 0x%08X.\n", wr, GetLastError());
            break;
		}
	}

cleanup:

	if (refresh_req_event)
	{
		CloseHandle( refresh_req_event );
	}

	if (refresh_req_done_event)
	{
		CloseHandle( refresh_req_done_event );
	}

	if (refresh_req_mx)
	{
		CloseHandle( refresh_req_mx );
	}

	if (sock_evt != NULL)
	{
		CloseHandle( sock_evt );
	}

	if (sock != INVALID_SOCKET)
	{
		closesocket( sock );
	}

    TRACE("Stopping monitor thread.\n");

    return 0;
}

static void WINAPI unload(DRIVER_OBJECT *driver)
{
    if (exit_event != NULL)
    {
        SetEvent( exit_event );
        if (monitor_thread != NULL)
        {
            if (WaitForSingleObject( monitor_thread, 5000 ) != WAIT_OBJECT_0)
                TerminateThread( monitor_thread, 0x42 );
            CloseHandle( monitor_thread );
        }
        CloseHandle( exit_event );
    }

    WSACleanup();

	if (g_cached_aa)
	{
		HeapFree( GetProcessHeap(), 0, g_cached_aa );
		g_cached_aa = NULL;
	}
	g_cached_aasize = 0;
}

NTSTATUS WINAPI DriverEntry(DRIVER_OBJECT *driver, UNICODE_STRING *path)
{
    OBJECT_ATTRIBUTES attr = {sizeof(attr)};
    WSADATA wsadata;
    NTSTATUS ret;

	printf("START TCPIP\n");

    driver->DriverUnload = unload;

    ret = WSAStartup( MAKEWORD(2,2), &wsadata );
    if (ret != 0)
    {
        ERR("WSASocketW fail = 0x%08X.\n", WSAGetLastError());
        return ret;
    }

    populate_tcp_ipv4_network_interface_registry_data();

    exit_event = CreateEventW( NULL, TRUE, FALSE, NULL );
    monitor_thread = CreateThread( NULL, 0, monitor_thread_proc, NULL, 0, NULL );
    return STATUS_SUCCESS;
}
